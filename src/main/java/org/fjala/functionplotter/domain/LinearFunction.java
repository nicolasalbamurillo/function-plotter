package org.fjala.functionplotter.domain;

public class LinearFunction implements IFunction {
    public float f(float x) {
        // f(x) = x
        return x;
    }
}

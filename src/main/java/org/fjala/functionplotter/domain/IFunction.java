package org.fjala.functionplotter.domain;

@FunctionalInterface
public interface IFunction {
    float f(float x);
}

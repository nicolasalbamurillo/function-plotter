package org.fjala.functionplotter.domain;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FunctionPlotter {
    private int imageWidth;
    private int imageHeight;
    private BufferedImage image;
    private Graphics2D g;

    public FunctionPlotter(int imageWidth, int imageHeight) {
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        this.g = this.image.createGraphics();
    }

    public void prepare() {
        printBackground(Color.WHITE);
        createSquared(Color.GRAY, 50);
        int middleHeight = imageHeight / 2;
        int middleWidth = imageWidth / 2;
        createXAxis(Color.RED, 50, middleHeight, middleWidth);
        createYAxis(Color.RED, 50, middleHeight, middleWidth);
    }

    private void printBackground(Color color) {
        this.g.setColor(color);
        this.g.fillRect(0, 0, this.imageWidth, this.imageHeight);
    }

    private void createSquared(Color color, int interval) {
        g.setColor(color);
        for (int i = 0; i <= imageWidth; i += interval) {
            g.drawLine(0, i, imageWidth, i);
        }
        for (int i = 0; i <= imageWidth; i += interval) {
            g.drawLine(i, 0, i, imageHeight);
        }
    }

    private void createXAxis(Color color, int interval, int middleHeight, int middleWidth) {
        g.setColor(color);
        g.drawLine(0, middleHeight, imageWidth, middleHeight);
        g.drawString("X", imageWidth - 8, middleHeight);
        Color words = Color.GRAY;
        g.setColor(words);
        for (int x = 0; x <= imageWidth; x += interval) {
            String text = String.valueOf(x - middleWidth);
            g.drawString(text, x + 1, middleHeight);
        }
    }

    private void createYAxis(Color color, int interval, int middleHeight, int middleWidth) {
        g.setColor(color);
        g.drawLine(middleWidth, 0, middleWidth, imageHeight);
        g.drawString("Y", middleWidth + 4, 10);
        Color words = Color.GRAY;
        g.setColor(words);
        for (int y = 0; y <= imageHeight; y += interval) {
            String text = String.valueOf(middleHeight - y);
            g.drawString(text, middleWidth + 1, y);
        }
    }

    private float transformX(float x) {
        int middleWidth = imageWidth / 2;
        return (x + middleWidth);
    }

    private float transformY(float y) {
        int middleHeight = imageHeight / 2;
        return (-y + middleHeight);
    }

    public void plot(IFunction function, Color color) {
        int circleDiameter = 8;
        float domainFrom = -this.imageWidth / 2;
        float domainTo = this.imageWidth / 2;

        float lastU = transformX(domainFrom);
        float lastV = transformY(function.f(domainFrom));

        this.g.setColor(color);

        for (float x = domainFrom + 1; x <= domainTo; x += 1) {
            float y = function.f(x);

            float u = transformX(x);
            float v = transformY(y);
            if (Float.isFinite(v) && Float.isFinite(lastV)) {
                this.g.drawLine((int) lastU, (int) lastV, (int) u, (int) v);
            } else if (Float.isNaN(v) && !Float.isNaN(lastV)) {
                int radius = circleDiameter / 2;
                int centerU = (int) (u - radius);
                int centerV = (int) (lastV - radius);
                g.drawOval(centerU, centerV, circleDiameter, circleDiameter);
            } else if (!Float.isNaN(v) && Float.isNaN(lastV)) {
                int radius = circleDiameter / 2;
                int centerU = (int) (lastU - radius);
                int centerV = (int) (v - radius);
                g.drawOval(centerU, centerV, circleDiameter, circleDiameter);
            }

            lastU = u;
            lastV = v;
        }
    }

    public void save(String pngPath) throws IOException {
        File file = new File(pngPath);
        ImageIO.write(this.image, "png", file);
    }
}

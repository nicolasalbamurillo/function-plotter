package org.fjala.functionplotter.domain;

public class CuadraticFunction implements IFunction {
    public float f(float x) {
        return x * x;
    }
}
